<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ForeignArena extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('arenas', function ($table) {
            $table->integer('invited_by')->unsigned()->index();
            $table->foreign('invited_by')->references('character_id')->on('characters')->onDelete('cascade');

            $table->integer('invited_to')->unsigned()->index();
            $table->foreign('invited_to')->references('character_id')->on('characters')->onDelete('cascade');;

            $table->integer('winned_by')->unsigned()->index()->nullable();
            $table->foreign('winned_by')->references('character_id')->on('characters')->onDelete('cascade');

            $table->integer('losed_by')->unsigned()->index()->nullable();
            $table->foreign('losed_by')->references('character_id')->on('characters')->onDelete('cascade');
        });



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('arenas', function ($table) {
            $table->dropForeign(['invited_by']);
            $table->dropForeign(['invited_to']);
            $table->dropForeign(['winned_by']);
            $table->dropForeign(['losed_by']);
        });
    }
}
