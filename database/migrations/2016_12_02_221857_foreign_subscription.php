<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ForeignSubscription extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('subscriptions', function ($table) {
            $table->integer('susbscribed_by')->unsigned()->index();
            $table->foreign('susbscribed_by')->references('character_id')->on('characters')->onDelete('cascade');
            $table->integer('guild_id')->unsigned()->index();
            $table->foreign('guild_id')->references('guild_id')->on('guilds')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('subscriptions', function ($table) {
            $table->dropForeign(['susbscribed_by']);
            $table->dropForeign(['guild_id']);
        });
    }
}
