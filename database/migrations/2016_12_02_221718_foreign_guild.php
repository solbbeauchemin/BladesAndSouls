<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ForeignGuild extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('guilds', function ($table) {
        $table->integer('guildLeader_id')->unsigned()->index()->nullable();
        $table->foreign('guildLeader_id')->references('character_id')->on('characters')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('guilds', function ($table) {
            $table->dropForeign(['guildLeader_id']);
        });
    }
}
