/**
 * Created by solbb on 2016-12-02.
 */

$('#modal-supprimer-article').on('show.bs.modal', function(e) {
    var userid = $(e.relatedTarget).data('id');
    $(e.currentTarget).find('input[name="art_id"]').val(userid);
});

$('#modal-supprimer-guilde').on('show.bs.modal', function(e) {
    var userid = $(e.relatedTarget).data('id2');
    $(e.currentTarget).find('input[name="art_id2"]').val(userid);
});