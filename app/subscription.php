<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class subscription extends Model
{
    protected $fillable = [
        'susbscibed_by', 'guild_id', 'confirmed','joined_at',
    ];

    public function user(){
        return $this->belongsTo('App\user');

    }

    public function guild(){
        return $this->belongsTo('App\guild');

    }
}
