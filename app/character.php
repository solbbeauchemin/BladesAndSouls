<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class character extends Model
{
    protected $fillable = [
        'user_id', 'nom', 'guild_id','classe','niveau','nbr_victoires','nbr_defaites',
    ];

    public $timestamps = false;


    public static function validate($input) {

        $rules = array(
            'nom' => 'required',
            'classe' => 'required',
            'niveau' => 'required|numeric|min:1|max:55',
        );
        return Validator::make($input, $rules);
    }


    public function user(){
        return $this->belongsTo('App\user');

    }

    public function subscription(){
        return $this->hasMany('App\subscription');
    }

    public function arena(){
        return $this->hasMany('App\arena');
    }

}
