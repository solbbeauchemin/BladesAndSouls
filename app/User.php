<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Validator;

class User extends Authenticatable
{
    use Notifiable;

    public static function validate($input) {

        $rules = array(
            'nom' => 'required|unique:users,name',
            'courriel' => 'required|email|unique:users,email',
            'pass' => 'required',
        );
        return Validator::make($input, $rules);
    }


    public function character(){
        return $this->hasMany('App\character');
    }



    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
