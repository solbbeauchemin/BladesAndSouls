<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class arena extends Model
{
    protected $fillable = [
        'invited_by', 'invited_to', 'confirmed','winned_by','losed_by','message'
    ];

    public $timestamps = false;

    public static function validate($input) {

        $rules = array(
            'invited_by' => 'required',
            'invited_to' => 'required',
        );
        return Validator::make($input, $rules);
    }


    public function user(){
        return $this->belongsToMany('App\user');

    }

}
