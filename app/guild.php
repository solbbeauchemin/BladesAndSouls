<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class guild extends Model
{
    protected $fillable = [
        'guildLeader_id', 'nom', 'description','faction',
    ];

    public $timestamps = false;

    public static function validate($input) {

        $rules = array(
            'nom' => 'required',
            'description' => 'required',
            'faction' => 'required'
        );
        return Validator::make($input, $rules);
    }

    public function character(){
        return $this->belongsTo('App\character');
    }

    public function characters(){
        return $this->hasMany('App\character');

    }
}
