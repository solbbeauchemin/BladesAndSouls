<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class log extends Model
{
    protected $fillable = [
        'email', 'url', 'methode','message',
    ];

    public $timestamps = false;
}
