<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\log;
use Carbon\Carbon;
use DB;
use App\User;
use App\character;
use App\guild;
use App\arena;

class ajouterLogs
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {



        $input=$request->All();
        //Si l'url contient, /utilisateur/enregistrer et si la validation est passée récupérer les informations et créer un log, sinon continuer.
        if(str_contains($request->getUri(), '/utilisateur/enregistrer'))
        {
            $validate = User::validate($input);

            if($validate->passes())
            {
                $Log= log::create(['email'=>'invité','url'=>$request->getUri(),'methode'=>$request->getMethod(),'message'=>$input['nom']." vient de se joindre à l'Arena pvp, ".Carbon::now('America/Toronto')]);
            }

        }
        //Si l'url contient, /personnage/creer et si la validation est passée récupérer les informations et créer un log, sinon continuer.
        if(str_contains($request->getUri(),'/personnage/creer'))
        {
            $validate = character::validate($input);
            if($validate->passes())
            {
                $user =DB::table('users')
                    ->where('id',Auth::id())
                    ->get();

                $Log= log::create(['email'=>$user[0]->email,'url'=>$request->getUri(),'methode'=>$request->getMethod(),'message'=>$user[0]->name." vient d'ajouter le personnage ".$input['nom']." ".$input['classe']." de niveau ".$input['niveau'].", ".Carbon::now('America/Toronto')]);
            }

        }
        //Si l'url contient, /personnage/creer et si la validation est passée récupérer les informations et créer un log, sinon continuer.
        if(str_contains($request->getUri(),'/guilde/creer'))
        {

            $validate = guild::validate($input);

            if($validate->passes())
            {
                $user =DB::table('users')
                    ->where('id',Auth::id())
                    ->get();


                $Log= log::create(['email'=>$user[0]->email,'url'=>$request->getUri(),'methode'=>$request->getMethod(),'message'=>$user[0]->name." vient d'ajouter la guilde ".$input['nom']." dans la faction ".$input['faction'].", ".Carbon::now('America/Toronto')]);
            }

        }

        //Si l'url contient, /arena/duel et si la validation est passée récupérer les informations et créer un log, sinon continuer.
        if(str_contains($request->getUri(),'/arena/duel'))
        {
            $validate = arena::validate($input);

            if($validate->passes())
            {
                $perso=DB::table('characters')
                    ->where('character_id',$input['invited_by'])
                    ->get();

                $enemie=DB::table('characters')
                    ->where('character_id',$input['invited_to'])
                    ->get();


                $user =DB::table('users')
                    ->where('id',Auth::id())
                    ->get();

                $Log= Log::create(['email'=>$user[0]->email,'url'=>$request->getUri(),'methode'=>$request->getMethod(),'message'=>$user[0]->name." vient de faire une demande de duel entre ".$perso[0]->nom." ".$perso[0]->classe." de niveau ".$perso[0]->niveau." contre ".$enemie[0]->nom." ".$enemie[0]->classe." de niveau ".$enemie[0]->niveau.", ".Carbon::now('America/Toronto')]);
            }


        }
        $response = $next($request);
        return $response;
    }
}
