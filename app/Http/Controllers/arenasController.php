<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\arena;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;


class arenasController extends Controller
{
    public function getLists()
    {
        //Récupérer les perso et leur guilde et l'entrer dans une session.
        $persos = DB::table('characters')
            ->leftJoin('guilds', 'characters.guild_id', '=', 'guilds.guild_id')
            ->where('characters.user_id', '=', Auth::id())
            ->select('characters.*', 'guilds.nom as guild_nom', 'guilds.guildLeader_id')
            ->get();

        session(['mesPersos' =>$persos]);

        //Récupérer les énemies et leur guilde et l'entrer dans une session.
        $enemies = DB::table('characters')
            ->leftJoin('guilds', 'characters.guild_id', '=', 'guilds.guild_id')
            ->where('characters.user_id', '<>', Auth::id())
            ->select('characters.*', 'guilds.nom as guild_nom', 'guilds.guildLeader_id')
            ->get();

        session(['mesEnemies' =>$enemies]);

        return view('arena/arenas');
    }

    public function inviterDuel(Request $request)
    {
        //Valider la requête
        $input = $request->all();

        $this->validate($request, [
            'invited_by' => 'required',
            'invited_to' => 'required',
        ]);


        //Récupérer l'envoyeur, le receveur et leurs personnages dans la demande de duel.
        $perso=DB::table('characters')
            ->where('character_id',$input['invited_by'])
            ->get();
        $envoyeur=DB::table('users')
            ->where('id',Auth::id())
            ->get();

        $enemie=DB::table('characters')
            ->where('character_id',$input['invited_to'])
            ->get();

        $receveur=DB::table('users')
            ->where('id',$enemie[0]->user_id)
            ->get();

        //Ajouter dans la base de données l'invitation.
        $arena= arena::create(['invited_by'=>$perso[0]->character_id,'invited_to'=>$enemie[0]->character_id,'message'=>"Vous avez reçu une demande de duel entre ".$perso[0]->nom." ".$perso[0]->classe." de niveau ".$perso[0]->niveau." contre ".$enemie[0]->nom." ".$enemie[0]->classe." de niveau ".$enemie[0]->niveau.", ".Carbon::now('America/Toronto')]);

        //Envoyer l'invitation par courriel.
        Mail::send('courriels.invitation',['nom'=>$perso[0]->nom,'classe'=>$perso[0]->classe,'niveau'=>$perso[0]->niveau,'dateTime'=>Carbon::now('America/Toronto'),'enemie'=>$enemie[0]->nom,'classeEnemie'=>$enemie[0]->classe,'niveauEnemie'=>$enemie[0]->niveau],function ($message)use($receveur,$envoyeur){
            $message->to($receveur[0]->email)->subject("Invitation à un duel!")->from($envoyeur[0]->email);
        });

        return view('arena/arenas');
    }

    public function afficherDuels()
    {
        //Récupérer les invitations reçues.
        $duels=DB::table('arenas')
            ->join('characters', 'arenas.invited_to', '=', 'characters.character_id')
            ->join('users', 'users.id', '=', 'characters.user_id')
            ->where('users.id', '=', Auth::id())
            ->select('arenas.*')
            ->get();


        return view('arena/mesInvitations')->with('duels',$duels);
    }


}
