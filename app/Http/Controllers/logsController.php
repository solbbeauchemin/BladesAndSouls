<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\log;
use Illuminate\Support\Facades\Auth;
use DB;

class logsController extends Controller
{
    public function afficherLogs()
    {
        //Récupérer les logs pour les afficher dans la page des actualités.
        $logs=DB::table('logs')
            ->get();

        return view('arena/actualites')->with('logs',$logs);

    }
}
