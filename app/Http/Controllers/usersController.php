<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class usersController extends Controller
{
    public function register(Request $request)
    {
        //Faire la validation.
        $input = $request->all();

        $this->validate($request, [
            'nom' => 'required|unique:users,name',
            'courriel' => 'required|email|unique:users,email',
            'pass' => 'required'
        ]);

        //Ajouter l'utilisateur dans la base de données.
        $user= User::create(['name'=>$input['nom'],'email'=>$input['courriel'],'password'=>hash::make($input['pass'])]);
        return view('arena/index');
    }

    public function username()
    {
        return 'username';
    }

    public function login(Request $request)
    {
        //Vérifier si l'utilisateur est dans la base de données.
        $input = $request->all();
        if (Auth::attempt(['name' => $input['user'], 'password' =>$input['pass']])) {

            //Récupérer les nouvelles listes des guildes et les entrer dans une session.
            $guilds = DB::table('guilds')
                ->get();

            session(['guilds' =>$guilds]);

            //Obtenir la liste des personnages et leur guilde et l'entrer dans une session.
            $persos = DB::table('characters')
                ->leftJoin('guilds', 'characters.guild_id', '=', 'guilds.guild_id')
                ->where('characters.user_id', '=', Auth::id())
                ->select('characters.*', 'guilds.nom as guild_nom', 'guilds.guildLeader_id')
                ->get();


            session(['mesPersos' =>$persos]);

            //Obtenir la liste des enemies et leur guilde et l'entrer dans une session.
            $enemies = DB::table('characters')
                ->leftJoin('guilds', 'characters.guild_id', '=', 'guilds.guild_id')
                ->where('characters.user_id', '<>', Auth::id())
                ->select('characters.*', 'guilds.nom as guild_nom', 'guilds.guildLeader_id')
                ->get();

            session(['mesEnemies' =>$enemies]);


        }
        return view('arena/salon');

    }

    public function logout(Request $request)
    {
        //Vider la session authentifier et retourner à la page d'acceuil.
        Auth::logout();
        return view('arena/acceuil');
    }

}
