<?php

namespace App\Http\Controllers;
use App\guild;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;

class guildsController extends Controller
{
   public function creer(Request $request)
   {

       //faire la validation.
       $input = $request->all();

       $this->validate($request, [
           'nom' => 'required',
           'description' => 'required',
           'faction' => 'required'
       ]);

        //Ajouter la guilde dans la base de données.
       $user= guild::create(['guildLeader_id'=>$input['leader_id'],'nom'=>$input['nom'],'description'=>$input['description'],'faction'=>$input['faction']]);

       //Mettre à jours la guilde du chef de guilde.
       $test=DB::table('characters')
           ->where('character_id',$input['leader_id'])
          ->update(['guild_id' => $user->id]);


       //Obtenir la liste des personnages et leur guilde et l'entrer dans une session.
       $persos = DB::table('characters')
           ->leftJoin('guilds', 'characters.guild_id', '=', 'guilds.guild_id')
           ->where('characters.user_id', '=', Auth::id())
           ->select('characters.*', 'guilds.nom as guild_nom', 'guilds.guildLeader_id')
           ->get();

       session(['mesPersos' =>$persos]);

       //Trouver les guildes disponibles et les entrer dans une session.
       $guilds = DB::table('guilds')
           ->get();

       session(['guilds' =>$guilds]);

       return view('arena/personnages');
   }


   public function join(Request $request)
   {
       $input = $request->all();

       //Modifier la guilde du personnage.
       DB::table('characters')
           ->where('character_id',$input['character'])
           ->update(['guild_id' => $input['guild']]);

       //Obtenir la liste des personnages et leur guilde et l'entrer dans une session.
       $persos = DB::table('characters')
           ->leftJoin('guilds', 'characters.guild_id', '=', 'guilds.guild_id')
           ->where('characters.user_id', '=', Auth::id())
           ->select('characters.*', 'guilds.nom as guild_nom', 'guilds.guildLeader_id')
           ->get();

       session(['mesPersos' =>$persos]);


       return view('arena/personnages');
   }

   public function mesGuildes()
    {
        //Récupérer toutes les guildes dont un de vos personnages est le chef.
        $mesguildes = DB::table('guilds')
            ->join('characters', 'guilds.guildLeader_id', '=', 'characters.character_id')
            ->join('users', 'users.id', '=', 'characters.user_id')
            ->where('users.id', '=', Auth::id())
            ->select('guilds.*', 'characters.nom as character_nom', 'users.id')
            ->get();


        return view('arena/mesguildes')->with('mesguildes',$mesguildes);


    }


    public function supprimer(Request $request)
    {
        $input = $request->all();

        //Supprimer la guilde sélectionné dans la base de données.
        DB::table('guilds')->where('guild_id',$input['guild_id'])->delete();


        //Récupérer toutes les guildes dont un de vos personnages est le chef.
        $mesguildes = DB::table('guilds')
            ->join('characters', 'guilds.guildLeader_id', '=', 'characters.character_id')
            ->join('users', 'users.id', '=', 'characters.user_id')
            ->where('users.id', '=', Auth::id())
            ->select('guilds.*', 'characters.nom as character_nom', 'users.id')
            ->get();




        //Obtenir la liste des personnages et leur guilde et l'entrer dans une session.
        $persos = DB::table('characters')
            ->leftJoin('guilds', 'characters.guild_id', '=', 'guilds.guild_id')
            ->where('characters.user_id', '=', Auth::id())
            ->select('characters.*', 'guilds.nom as guild_nom', 'guilds.guildLeader_id')
            ->get();

        session(['mesPersos' =>$persos]);

        return view('arena/mesguildes')->with('mesguildes',$mesguildes);


    }

    public function modifier(Request $request)
    {
        //Faire la validation.
        $input = $request->all();

        $this->validate($request, [
            'nom' => 'required',
            'description' => 'required',
            'faction' => 'required'
        ]);

        //Mettre les nouvelles données de la guilde dans la base de données.
        DB::table('guilds')
            ->where('guild_id',$input['guild_id'])
            ->update(['nom' => $input['nom'],'description'=>$input['description'],'faction'=>$input['faction']]);

        //Obtenir les nouvelles informations sur les guildes dont un de vos personnages est le chef.
        $mesguildes = DB::table('guilds')
            ->join('characters', 'guilds.guildLeader_id', '=', 'characters.character_id')
            ->join('users', 'users.id', '=', 'characters.user_id')
            ->where('users.id', '=', Auth::id())
            ->select('guilds.*', 'characters.nom as character_nom', 'users.id')
            ->get();

        //Obtenir la liste des personnages et leur guilde et l'entrer dans une session.
        $persos = DB::table('characters')
            ->leftJoin('guilds', 'characters.guild_id', '=', 'guilds.guild_id')
            ->where('characters.user_id', '=', Auth::id())
            ->select('characters.*', 'guilds.nom as guild_nom', 'guilds.guildLeader_id')
            ->get();

        session(['mesPersos' =>$persos]);


        return view('arena/mesguildes')->with('mesguildes',$mesguildes);
    }

    public function guildeModifier($id)
    {
        //Obtenir les informations de la guilde à modifier.
        $guild= DB::table('guilds')
            ->where('guild_id',$id)
            ->get();

        return view('arena/modifierguilde')->with('guild',$guild);
    }


}
