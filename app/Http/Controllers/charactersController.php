<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\character;
use Illuminate\Support\Facades\Auth;
use DB;

class charactersController extends Controller
{

   public function creer(Request $request)
   {
       //Faire la validation.
       $input = $request->all();

       $this->validate($request, [
           'nom' => 'required',
           'classe' => 'required',
           'niveau' => 'required|numeric|min:1|max:55'
       ]);

        //Ajouter le personnage dans la base de données.
       $user= character::create(['user_id'=>Auth::id(),'nom'=>$input['nom'],'classe'=>$input['classe'],'niveau'=>$input['niveau'],'nbr_victoires'=>0,'nbr_defaites'=>0]);


       //Obtenir la liste des personnages et leur guilde et l'entrer dans une session.
       $persos = DB::table('characters')
           ->leftJoin('guilds', 'characters.guild_id', '=', 'guilds.guild_id')
           ->where('characters.user_id', '=', Auth::id())
           ->select('characters.*', 'guilds.nom as guild_nom', 'guilds.guildLeader_id')
           ->get();

       session(['mesPersos' =>$persos]);

       return view('arena/personnages');
   }


    public function modifier(Request $request)
    {
        //Faire la validation.
        $input = $request->all();

        $this->validate($request, [
            'nom' => 'required',
            'classe' => 'required',
            'niveau' => 'required|numeric|min:1|max:55'
        ]);

        //Modifier le personnage dans la base de données.
        DB::table('characters')
            ->where('character_id',$input['character_id'])
            ->update(['nom' => $input['nom'],'classe'=>$input['classe'],'niveau'=>$input['niveau']]);

        //Obtenir la liste des personnages et leur guilde et l'entrer dans une session.
        $persos = DB::table('characters')
            ->leftJoin('guilds', 'characters.guild_id', '=', 'guilds.guild_id')
            ->where('characters.user_id', '=', Auth::id())
            ->select('characters.*', 'guilds.nom as guild_nom', 'guilds.guildLeader_id')
            ->get();

        session(['mesPersos' =>$persos]);

        return view('arena/personnages');
    }


    public function persoModifier($id)
    {
        //Envoyer à la vue de modification les informations du personnage.
       $character= DB::table('characters')
            ->where('character_id',$id)
            ->get();

        return view('arena/modifierPersonnage')->with('character',$character);
    }



    public function supprimer(Request $request)
    {
        $input = $request->all();

        //Supprimer le personnage sélectionné dans la base de données.
        DB::table('characters')->where('character_id',$input['art_id'])->delete();

        //Obtenir la liste des personnages et leur guilde et l'entrer dans une session.
        $persos = DB::table('characters')
            ->leftJoin('guilds', 'characters.guild_id', '=', 'guilds.guild_id')
            ->where('characters.user_id', '=', Auth::id())
            ->select('characters.*', 'guilds.nom as guild_nom', 'guilds.guildLeader_id')
            ->get();

        session(['mesPersos' =>$persos]);

        return view('arena/personnages');

    }


}
