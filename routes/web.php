<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

//Si l'utilisateur est authentifié aller au salon, sinon aller à l'acceuil.
Route::get('/', function (Request $request) {
    //Auth::logout();
    //$request->session()->flush();
    if(Auth::check())
        return view('arena.salon');
    else
        return view('arena.acceuil');
});

//Si l'utilisateur est authentifié aller au salon, sinon aller à l'acceuil.
Route::get('/utilisateur/enregistrer', function () {
    if(Auth::check())
        return view('arena.salon');
    else
        return view('arena.acceuil');
});

//Enregistrer l'utilisateur dans la base de données.
Route::post('/utilisateur/enregistrer','usersController@register', function () {

})->middleware('log');

//Si l'utilisateur est authentifié aller au salon, sinon aller à l'acceuil.
Route::get('/utilisateur/connexion', function () {
    if(Auth::check())
        return view('arena.salon');
    else
        return view('arena.acceuil');
});

//Connecter l'utilisateur.
Route::post('/utilisateur/connexion','usersController@login', function () {

});

//Déconnecter l'utilisateur.
Route::get('/utilisateur/deconnexion','usersController@logout', function () {

})->middleware('auth');

//Obtenir la liste des personnages.
Route::get('/personnages/', function () {
    return view('arena.personnages');
})->middleware('auth');

//Obtenir la vue modifier.
Route::get('/personnage/modifier/{id}','charactersController@persoModifier', function ($id) {

})->where('id', '[0-9]+')->middleware('auth');

//Modifier le personnage.
Route::patch('/personnage/modifier/','charactersController@modifier', function () {

})->middleware('auth');

//Retour à la liste des personnages.
Route::get('/personnage/supprimer/', function () {
    return view('arena.personnages');
})->middleware('auth');

//Supprimer le personnage.
Route::delete('/personnage/supprimer/','charactersController@supprimer', function () {

})->middleware('auth');

//Retour à la liste des personnages.
Route::get('/personnage/creer', function () {
    return view('arena.ajouterPersonnage');
})->middleware('auth');

//Ajouter le personnage dans la base de données.
Route::put('/personnage/creer','charactersController@creer', function () {

})->middleware('log','auth');

//Obtenir la vue création de guilde avec l'id du créateur.
Route::get('/guilde/creer/{id}', function ($id) {
    return view('arena.guilds')->with('id',$id);
})->where('id', '[0-9]+')->middleware('auth');;

//Créer la guilde.
Route::put('/guilde/creer','guildsController@creer', function () {

})->middleware('log','auth');

//Obtenir la liste des guildes dont un de vos personnage est le chef.
Route::get('/guilde/mesguildes','guildsController@mesGuildes', function () {

})->middleware('auth');;

//Obtenir la vue pour joindre une guilde.
Route::get('/guilde/joindre/{id}', function ($id) {
    return view('arena.joinguilds')->with('id',$id);
})->where('id', '[0-9]+')->middleware('auth');;

//Modifier la guilde du personnage.
Route::patch('/guilde/joindre/','guildsController@join', function () {

})->middleware('auth');

//Retour à la liste de mes guildes.
Route::get('/guilde/supprimer/', function () {
    return view('arena.mesguildes');
})->middleware('auth');;

//Supprimer la guilde.
Route::delete('/guilde/supprimer/','guildsController@supprimer', function () {

})->middleware('auth');;

//Obtenir la vue de modification d'une guilde avec ses informations.
Route::get('/guilde/modifier/{id}','guildsController@guildeModifier', function ($id) {

})->where('id', '[0-9]+')->middleware('auth');;

//Modifier la guilde.
Route::patch('/guilde/modifier/','guildsController@modifier', function () {

})->middleware('auth');;

//Afficher la liste des personnages disponibles pour un duel.
Route::get('/arenas/','arenasController@getLists', function () {
    return view('arena.arenas');
})->middleware('auth');;

//Retourner à la liste des personnages disponibles pour un duel.
Route::get('/arena/duel', function () {
        return view('arena.arenas');
})->middleware('auth');;

//Inviter un personnage à un duel et lui envoyer le courriel correspondant.
Route::put('/arena/duel','arenasController@inviterDuel', function () {

})->middleware('log','auth');

//Obtenir la liste des actualités.
Route::get('/actualites', 'logsController@afficherLogs', function (){

})->middleware('auth');

//Obtenir la liste de mes inviations à un duel.
Route::get('arenas/invitations', 'arenasController@afficherDuels', function (){

})->middleware('auth');

Auth::routes();

