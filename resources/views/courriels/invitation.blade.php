<h4>Quelqu'un vous à invité à un duel à {{$dateTime}}!</h4>
<h4>Le personnage du nom de {{$nom}}</h4>
<h4>{{$classe}} de niveau {{$niveau}}</h4>
<h4>Vous convoque à l'arena </h4>
<h4>Contre {{$enemie}}</h4>
<h4>{{$classeEnemie}} de niveau {{$niveauEnemie}}</h4>
<h4>si vous acceptez, veuillez communiquer avec lui pour arranger la date et l'heure du duel.</h4>