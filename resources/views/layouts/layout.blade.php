<!DOCTYPE html>
<html lang="en">
<head>
    <title> Arena pvp</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/gif/png" href="{{elixir('image/bns-logo.png')}}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{elixir('css/style.css')}}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>





</head>
<body>

<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <a class="navbar-brand " href="#">  <img class="img-responsive" src="{{elixir('image/bns-logo.png')}}"></a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">
                <li class="active"><a href="../../">Acceuil</a></li>
                @if(Auth::check())
                <li><a href="../../personnages">Mes personnages</a></li>
                <li><a href="../../arenas">Duels</a></li>
                <li><a href="../../arenas/invitations">Mes invitations</a></li>
                <li><a href="../../guilde/mesguildes">Mes guildes</a></li>
                <li><a href="../../actualites">Actualités</a></li>
                @endif
            </ul>
            <ul class="nav navbar-nav navbar-right">

                @if(Auth::check())

                        <li><a href="../utilisateur/deconnexion"><span class="glyphicon glyphicon-log-out"></span> Déconnexion </a></li>

                @else
                            <li><a href="#" data-toggle="modal" data-target="#registerModal"><span class="glyphicon glyphicon-user"></span>S'enregistrer</a></li>
                            <li><a href="#" data-toggle="modal" data-target="#connectModal"><span class="glyphicon glyphicon-log-in"></span>Connexion</a></li>
                @endif

            </ul>
        </div>
    </div>
</nav>







<div class="modal fade" id="registerModal">
    <div class="modal-dialog">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">S'enregistrer</h4>
            </div>
            <div class="modal-body">
                <form method="post" action="../utilisateur/enregistrer" class="form-group">
                    {{csrf_field()}}
                    <label for="exampleInputEmail1">Nom d'utilisateur</label>
                    <input class="form-control" name="nom" id="exampleInputUser" placeholder="Nom" type="text">
                    <label for="exampleInputEmail1">Courriel</label>
                    <input class="form-control" name="courriel" id="exampleInputUser" placeholder="Courriel" type="text">
                    <label for="exampleInputPassword1">Mot de passe</label>
                    <input class="form-control" name="pass" id="exampleInputPass" placeholder="Mot de passe" type="text">
                    <div class="modal-footer">
                        <a href="#" data-dismiss="modal" class="btn">Fermer</a>
                        <button type="submit"  name="engUser" class="btn btn-primary">S'enregistrer</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="connectModal">
    <div class="modal-dialog">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Se connecter</h4>
            </div>
            <div class="modal-body">
                <form method="post" action="../utilisateur/connexion" class="form-group">
                    {{csrf_field()}}
                    <label for="exampleInputEmail1">Nom d'utilisateur</label>
                    <input class="form-control" name="user" id="exampleInputUser" placeholder="Nom" type="text">
                    <label for="exampleInputPassword1">Mot de passe</label>
                    <input class="form-control" name="pass" id="exampleInputPass" placeholder="Mot de passe" type="text">
                    <div class="modal-footer">
                        <a href="#" data-dismiss="modal" class="btn">Fermer</a>
                        <button type="submit"  name="engUser" class="btn btn-primary">Connexion</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>




@yield('hautDePage')
<div class="col-sm-9">
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
</div>
@yield('contenu')

<footer class="container-fluid ">
    <p>Projet final présenté par Sol Bergeron Beauchemin dans le cadre du cours 420-545-HU BASES DE DONNÉES II</p>
</footer>

<script src="{{elixir('js/script.js')}}"></script>

</body>
</html>
