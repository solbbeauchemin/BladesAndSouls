@extends('layouts.layout')

@section('hautDePage')
    <div>
        <h1>
            Mes invitations
        </h1>
    </div>
@endsection







@section('contenu')
    <div class="container">
        <br>
        <div class="row">
            <div class="col-sm-6">
                <hr>
                @foreach($duels as $duel)
                    {{$duel->message}}
                    <br>
                @endforeach
                <hr>

            </div>
            <div class="col-sm-4 col-sm-offset-2">
                <div class="bnsLogo">
                    <img src="{{elixir('image/blade-and-soul.png')}}" class="img-responsive" style="width:100%" alt="Image">
                </div>
                <div class="bnsLogo">
                    <img src="{{elixir('image/ncsoft.jpg')}}" class="img-responsive" style="width:100%" alt="Image">
                </div>
            </div>
        </div>
    </div><br>





@endsection
