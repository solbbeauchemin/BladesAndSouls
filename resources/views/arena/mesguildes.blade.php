@extends('layouts.layout')

@section('hautDePage')
    <div>
        <h1>
            Mes guildes
        </h1>
    </div>
@endsection







@section('contenu')
    <div class="container text-center">
        <br>
        <div class="row">
            <div class="col-sm-3">

                @foreach($mesguildes as $guild)
                       <div class="well">
                        <label>Nom:</label><label>{{$guild->nom}}</label>
                        <br>
                        <label>Description:</label><label>{{$guild->description}}</label>
                        <br>
                        <label>Chef de guilde</label><label>{{$guild->character_nom}}</label>
                        @if($guild->faction =="Crimson")
                            <img class="faction-logo" src="{{elixir('image/CrimsonLegion.png')}}">
                        @else
                            <img class="faction-logo" src="{{elixir('image/cerulean.png')}}">
                        @endif
                        <br>
                           <form method="post" action="../guilde/supprimer">
                               {{csrf_field()}}
                               <input type="hidden" name="guildLeader_id" value="{{$guild->guildLeader_id}}">
                               <input type="hidden" name="guild_id" value="{{$guild->guild_id}}">
                               <input type="hidden" name="_method" value="DELETE">
                               <button type="submit" name="supprimer" class="btn btn-danger btn-block"><i class="fa fa-trash"></i> Supprimer</button>
                           </form>
                    <a href="../../guilde/modifier/{{$guild->guild_id}}" class="btn btn-warning btn-block">Modifier la guilde</a>
                       </div>
                @endforeach
            </div>
            <div class="col-sm-3">

            </div>



            <div class="modal fade" id="modal-supprimer-guilde">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Supprimer la guilde</h4>
                        </div>
                        <div class="modal-body">
                            Êtes-vous certain de vouloir supprimer cette guilde?
                        </div>
                        <div class="modal-footer">
                            <form method="post" action="../guilde/supprimer">
                                {{csrf_field()}}
                                <input type="hidden" name="art_id2" value="">
                                <input type="hidden" name="_method" value="DELETE">
                                <button type="submit" name="supprimer" class="btn btn-danger"><i class="fa fa-trash"></i> Supprimer</button>
                            </form>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.mo






            <div class="col-sm-4 col-sm-offset-2">
                <div class="bnsLogo">
                    <img src="{{elixir('image/blade-and-soul.png')}}" class="img-responsive" style="width:100%" alt="Image">
                </div>
                <div class="bnsLogo">
                    <img src="{{elixir('image/ncsoft.jpg')}}" class="img-responsive" style="width:100%" alt="Image">
                </div>
            </div>
        </div>
    </div><br>

@endsection
