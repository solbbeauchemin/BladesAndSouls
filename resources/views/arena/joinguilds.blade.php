@extends('layouts.layout')

@section('hautDePage')
    <div>
        <h1>
            Joindre une guilde
        </h1>
    </div>
@endsection







@section('contenu')
    <div class="container text-center">
        <br>
        <div class="row">
            <div class="col-sm-3">

                @foreach(session('guilds') as $guild)
                    <form method="post" action="../../guilde/joindre">
                        {{csrf_field()}}
                        <input type="hidden" name="_method" value="PATCH">
                        <input type="hidden" value="{{$id}}" name="character">
                        <input type="hidden" value="{{$guild->guild_id}}" name="guild">
                        <label>Nom:</label><label>{{$guild->nom}}</label>
                        <br>
                        <label>Description:</label><label>{{$guild->description}}</label>
                        <br>
                        @if($guild->faction =="Crimson")
                            <img class="faction-logo" src="{{elixir('image/CrimsonLegion.png')}}">
                        @else
                            <img class="faction-logo" src="{{elixir('image/cerulean.png')}}">
                        @endif
                        <br>
                        <input type="submit" name="join" value="Join" class="btn btn-success">
                    </form>
                @endforeach
            </div>
            <div class="col-sm-3">


            </div>
            <div class="col-sm-4 col-sm-offset-2">
                <div class="bnsLogo">
                    <img src="{{elixir('image/blade-and-soul.png')}}" class="img-responsive" style="width:100%" alt="Image">
                </div>
                <div class="bnsLogo">
                    <img src="{{elixir('image/ncsoft.jpg')}}" class="img-responsive" style="width:100%" alt="Image">
                </div>
            </div>
        </div>
    </div><br>

@endsection
