@extends('layouts.layout')

@section('hautDePage')
    <div>
        <h1>
            Mes personnages
        </h1>


    </div>
@endsection








@section('contenu')
    <div class="container text-center">
        <br>
        <div class="row">
            <div class="col-sm-3">
                <div class="form-group">

                    <a href="../personnage/creer" class="btn btn-danger">Ajouter un personnage</a>

                </div>



                @if(Session::has('mesPersos'))
                    @foreach(session('mesPersos') as $perso)
                        <div class="well">
                            <label>Nom: </label><label>{{$perso->nom}}</label>
                            </br>
                            <label>Classe: </label><label>{{$perso->classe}}</label>
                            </br>
                            <label>Niveau: </label><label>{{$perso->niveau}}</label>
                            </br>
                            <label>Guilde:</label><label>{{$perso->guild_nom}}</label>

                            @if($perso->character_id==$perso->guildLeader_id)
                                </br><label>Chef de guilde</label>
                            @endif
                            </br>
                            <label>Nombre de victoires: </label><label>{{$perso->nbr_victoires}}</label>
                            </br>
                            <label>Nombre de défaites: </label><label>{{$perso->nbr_victoires}}</label>

                            @if($perso->guild_id==null)
                                <a href="../../guilde/creer/{{$perso->character_id}}" class="btn btn-primary btn-block">Créer une guilde</a>
                                <a href="../../guilde/joindre/{{$perso->character_id}}" class="btn btn-success btn-block">Joindre une guilde</a>
                                <a href="#modal-supprimer-perso" data-id="{{$perso->character_id}}" name="" class="btn btn-danger btn-block" data-toggle="modal" data-target="#modal-supprimer-article"><i class="fa fa-trash"></i> Supprimer le personnage</a>
                                <a href="../../personnage/modifier/{{$perso->character_id}}" class="btn btn-warning btn-block">Modifier le personnage</a>
                            @elseif($perso->character_id==$perso->guildLeader_id)
                                <a href="#modal-supprimer-perso" data-id="{{$perso->character_id}}" name="" class="btn btn-danger btn-block" data-toggle="modal" data-target="#modal-supprimer-article"><i class="fa fa-trash"></i> Supprimer le personnage</a>
                                <a href="../../personnage/modifier/{{$perso->character_id}}" class="btn btn-warning btn-block">Modifier le personnage</a>
                            @else
                                <a href="../../guilde/joindre/{{$perso->character_id}}" class="btn btn-success btn-block">Joindre une guilde</a>
                                <a href="#modal-supprimer-perso" data-id="{{$perso->character_id}}" name="" class="btn btn-danger btn-block" data-toggle="modal" data-target="#modal-supprimer-article"><i class="fa fa-trash"></i> Supprimer le personnage</a>
                                <a href="../../personnage/modifier/{{$perso->character_id}}" class="btn btn-warning btn-block">Modifier le personnage</a>

                            @endif
                        </div>
                        <br>
                    @endforeach
                @endif
            </div>


            <div class="modal fade" id="modal-supprimer-article">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Supprimer le personnage</h4>
                        </div>
                        <div class="modal-body">
                            Êtes-vous certain de vouloir supprimer ce personnage?
                        </div>
                        <div class="modal-footer">
                            <form method="post" action="../personnage/supprimer">
                                {{csrf_field()}}
                                <input type="hidden" name="art_id" value="">
                                <input type="hidden" name="_method" value="DELETE">
                                <button type="submit" name="supprimer" class="btn btn-danger"><i class="fa fa-trash"></i> Supprimer</button>
                            </form>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->








            <div class="col-sm-3">


            </div>
            <div class="col-sm-4 col-sm-offset-2">
                <div class="bnsLogo">
                    <img src="{{elixir('image/blade-and-soul.png')}}" class="img-responsive" style="width:100%" alt="Image">
                </div>
                <div class="bnsLogo">
                    <img src="{{elixir('image/ncsoft.jpg')}}" class="img-responsive" style="width:100%" alt="Image">
                </div>
            </div>
        </div>
    </div><br>

@endsection
