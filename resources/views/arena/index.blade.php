@extends('layouts.layout')

@section('hautDePage')
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
            <li data-target="#myCarousel" data-slide-to="3"></li>
            <li data-target="#myCarousel" data-slide-to="4"></li>
            <li data-target="#myCarousel" data-slide-to="5"></li>
            <li data-target="#myCarousel" data-slide-to="6"></li>
            <li data-target="#myCarousel" data-slide-to="7"></li>
            <li data-target="#myCarousel" data-slide-to="8"></li>
            <li data-target="#myCarousel" data-slide-to="9"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <img src="{{elixir('image/Races.jpg')}}" alt="Races">
                <div class="carousel-caption">
                    <h3>Races</h3>
                    <p>Les 4 races disponibles</p>
                </div>
            </div>

            <div class="item">
                <img src="{{elixir('image/Assassin.jpg')}}" alt="Assassin">
                <div class="carousel-caption">
                    <h3>Assassin</h3>
                    <p>Le maître de l'ombre</p>
                </div>
            </div>

            <div class="item">
                <img src="{{elixir('image/BladeDancer.jpg')}}" alt="BladeDancer">
                <div class="carousel-caption">
                    <h3>Blade dancer</h3>
                    <p>La tempête de lames</p>
                </div>
            </div>

            <div class="item">
                <img src="{{elixir('image/BladeMaster.jpg')}}" alt="BladeMaster">
                <div class="carousel-caption">
                    <h3>Blade master</h3>
                    <p>Le maître d'armes</p>
                </div>
            </div>

            <div class="item">
                <img src="{{elixir('image/Destroyer.jpg')}}" alt="Destroyer">
                <div class="carousel-caption">
                    <h3>Destroyer</h3>
                    <p>Le destructeur</p>
                </div>
            </div>

            <div class="item">
                <img src="{{elixir('image/ForceMaster.jpg')}}" alt="ForceMaster">
                <div class="carousel-caption">
                    <h3>Force master</h3>
                    <p>Le champion de la magie</p>
                </div>
            </div>

            <div class="item">
                <img src="{{elixir('image/KungFuMaster.jpg')}}" alt="KungFuMaster">
                <div class="carousel-caption">
                    <h3>Kung fu master</h3>
                    <p>Le maître des arts martiaux</p>
                </div>
            </div>

            <div class="item">
                <img src="{{elixir('image/Soulfighter.jpg')}}" alt="SoulFighter">
                <div class="carousel-caption">
                    <h3>Soul fighter</h3>
                    <p>Le maître du combat psychique</p>
                </div>
            </div>

            <div class="item">
                <img src="{{elixir('image/Summoner.jpg')}}" alt="Summoner">
                <div class="carousel-caption">
                    <h3>Summoner</h3>
                    <p>Le spécialiste des invocations</p>
                </div>
            </div>

            <div class="item">
                <img src="{{elixir('image/Warlock.jpg')}}" alt="Warlock">
                <div class="carousel-caption">
                    <h3>Warlock</h3>
                    <p>Maître de l'art obscure</p>
                </div>
            </div>
        </div>


        <!-- Left and right controls -->
        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
@endsection







@section('contenu')
    <div class="container text-center">
        <h3>What We Do</h3><br>
        <div class="row">
            <div class="col-sm-3">
                <img src="{{elixir('image/cerulean.png')}}" class="img-responsive" style="width:100%" alt="Image">
                <p>Current Project</p>
            </div>
            <div class="col-sm-3">
                <img src="{{elixir('image/CrimsonLegion.png')}}" class="img-responsive" style="width:100%" alt="Image">
                <p>Project 2</p>
            </div>
            <div class="col-sm-4 col-sm-offset-2">
                <div class="bnsLogo">
                    <img src="{{elixir('image/blade-and-soul.png')}}" class="img-responsive" style="width:100%" alt="Image">
                </div>
                <div class="bnsLogo">
                    <img src="{{elixir('image/ncsoft.jpg')}}" class="img-responsive" style="width:100%" alt="Image">
                </div>
            </div>
        </div>
    </div><br>

@endsection
