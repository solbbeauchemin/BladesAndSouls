@extends('layouts.layout')

@section('hautDePage')
    <div>
        <h1>
            Modifier un personnage
        </h1>
    </div>
@endsection


@section('contenu')
    <div class="container text-center">
        <br>
        <div class="row">
            <div class="col-sm-3">
                <form action="../../guilde/modifier" method="post" role="form">
                    <input type="hidden" name="_method" value="PATCH">
                    <input type="hidden" name="guild_id" value="{{$guild[0]->guild_id}}">
                    {{csrf_field()}}
                    <label>Nom</label>
                    <div class="form-group">
                        <input type="text" id="txtNom" name="nom" placeholder="Nom"  value="{{$guild[0]->nom}}" class="form-control" >
                    </div>

                    <label>Description</label>
                    <div class="form-group">
                        <textarea name="description" class="form-control" rows="3" required>{{$guild[0]->description}}</textarea>
                    </div>

                    <label>Faction(Crimson-Cerulean)</label>
                    <div class="form-group">
                        <label class="radio-inline" ><input type="radio" name="faction" value="Crimson">
                            <img class="faction-logo" src="{{elixir('image/CrimsonLegion.png')}}" /></label>

                        <label class="radio-inline"><input type="radio" name="faction" value="Cerulean">
                            <img class="faction-logo" src="{{elixir('image/cerulean.png')}}" /></label>
                    </div>
                    <button type="Submit" class="btn btn-warning">Modifier</button>
                </form>

            </div>
            <div class="col-sm-3">


            </div>
            <div class="col-sm-4 col-sm-offset-2">
                <div class="bnsLogo">
                    <img src="{{elixir('image/blade-and-soul.png')}}" class="img-responsive" style="width:100%" alt="Image">
                </div>
                <div class="bnsLogo">
                    <img src="{{elixir('image/ncsoft.jpg')}}" class="img-responsive" style="width:100%" alt="Image">
                </div>
            </div>
        </div>
    </div><br>

@endsection