@extends('layouts.layout')

@section('hautDePage')
   <h1>Participer à un duel</h1>
@endsection







@section('contenu')
    <div class="container text-center">
        <h3>Salon des duels</h3>
        <form method="post" action="../../arena/duel">
            {{csrf_field()}}
            <input type="hidden" name="_method" value="PUT">
            <button type="submit" class="btn btn-warning">
                <i class="glyphicon glyphicon-envelope"></i> Envoyer une demande de duel
            </button>
        <div class="row">

                <br>
            <div class="col-sm-3">

            <h4>Mes personnages</h4>
            @foreach(session('mesPersos') as $perso)
                    <div class="well">
                    <input type="radio" name="invited_by" value="{{$perso->character_id}}">
                    <label>Nom: </label><label>{{$perso->nom}}</label>
                    </br>
                    <label>Classe: </label><label>{{$perso->classe}}</label>
                    </br>
                    <label>Niveau: </label><label>{{$perso->niveau}}</label>
                    </br>
                    <label>Guilde:</label><label>{{$perso->guild_nom}}</label>
                    </br>
                    <label>Nombre de victoires: </label><label>{{$perso->nbr_victoires}}</label>
                    </br>
                    <label>Nombre de défaites: </label><label>{{$perso->nbr_victoires}}</label>
                    </div>
            @endforeach
            </div>

            <div class="col-sm-3">
                <h4>Disponnible pour un duel</h4>
                @foreach(session('mesEnemies') as $enemie)
                    <div class="well">
                    <input type="radio" name="invited_to" value="{{$enemie->character_id}}">
                    <label>Nom: </label><label>{{$enemie->nom}}</label>
                    </br>
                    <label>Classe: </label><label>{{$enemie->classe}}</label>
                    </br>
                    <label>Niveau: </label><label>{{$enemie->niveau}}</label>
                    </br>
                    <label>Guilde:</label><label>{{$enemie->guild_nom}}</label>
                    </br>
                    <label>Nombre de victoires: </label><label>{{$enemie->nbr_victoires}}</label>
                    </br>
                    <label>Nombre de défaites: </label><label>{{$enemie->nbr_victoires}}</label>
                    </div>
                @endforeach
            </div>


            <div class="col-sm-4 col-sm-offset-2">
                <div class="bnsLogo">
                    <img src="{{elixir('image/blade-and-soul.png')}}" class="img-responsive" style="width:100%" alt="Image">
                </div>
                <div class="bnsLogo">
                    <img src="{{elixir('image/ncsoft.jpg')}}" class="img-responsive" style="width:100%" alt="Image">
                </div>
            </div>


        </div>
        </form>
    </div><br>

@endsection
