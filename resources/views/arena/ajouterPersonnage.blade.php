@extends('layouts.layout')

@section('hautDePage')
    <div>
        <h1>
            Ajout d'un personnage
        </h1>
    </div>
@endsection







@section('contenu')
    <div class="container text-center">
        <br>
        <div class="row">
            <div class="col-sm-3">
                <form action="../../personnage/creer" method="post" role="form">
                    <input type="hidden" name="_method" value="PUT">
                    {{csrf_field()}}
                    <label>Nom</label>
                    <div class="form-group">
                        <input type="text" id="txtNom" name="nom" placeholder="Nom"  class="form-control" >
                    </div>

                    <div class="form-group">
                        <div class="form-group">
                            <label for="s">Choisissez votre classe</label>
                            <select class="form-control" id="sel1" name="classe">
                                <option>Assassin</option>
                                <option>Blade Master</option>
                                <option>Force Master</option>
                                <option>Summoner</option>
                                <option>Kung Fu Master</option>
                                <option>Warlock</option>
                                <option>Blade Dancer</option>
                            </select>
                        </div>
                    </div>

                    <label>Niveau</label>
                    <div class="form-group">
                        <input type="number" min="1" max="55" id="txtNiveau" name="niveau" value="1"  class="form-control" >
                    </div>


                    <button type="Submit" class="btn btn-success">Ajouter</button>
                </form>

            </div>
            <div class="col-sm-3">


            </div>
            <div class="col-sm-4 col-sm-offset-2">
                <div class="bnsLogo">
                    <img src="{{elixir('image/blade-and-soul.png')}}" class="img-responsive" style="width:100%" alt="Image">
                </div>
                <div class="bnsLogo">
                    <img src="{{elixir('image/ncsoft.jpg')}}" class="img-responsive" style="width:100%" alt="Image">
                </div>
            </div>
        </div>
    </div><br>

@endsection